<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use thiagoalessio\TesseractOCR\TesseractOCR;

Route::get('/','OCRController@index');
Route::post('/toy','OCRController@selectFile');
Route::post('/show','OCRController@show');
