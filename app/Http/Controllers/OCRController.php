<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use thiagoalessio\TesseractOCR\TesseractOCR;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class OCRController extends Controller
{
    //
    public function index(){
//            $test = (new TesseractOCR('test.png'))->run();
        $client= new Client();
//        https://api.ocr.space/parse/imageurl?apikey=helloworld&url=http://i.imgur.com/fwxooMv.png
        $api ='https://api.ocr.space/parse/imageurl?';
        $key ='2d94da389888957';
        $imgUrl ='https://raw.githubusercontent.com/Balalonks/tesseract-ocr-for-php/master/tests/EndToEnd/images/text.png';
        $apiUrl = $api."apikey=".$key."&url=".$imgUrl;
        return view('welcome');
    }

    public function selectFile(Request $request){
        $file = $request->file('image');
        $file_name = $file->getClientOriginalName();
        $file_path = 'test';
        $file->move($file_path.'/',$file->getClientOriginalName());
        $image_src = $file_path.'/'.$file_name;
        $ocr = $this->ocr($file_path,$file_name);
        return view('show',compact('image_src','ocr'));
    }

    public function ocr($path,$name){
        $client = new Client();
        $api ='https://api.ocr.space/parse/image';
        $key ='2d94da389888957';
        try {
            $res = $client->request('POST', env('FRONT_IP').'/show', [
                'headers'=> [
                    'apikey'=>$key
                ],
                'multipart' => [
                    [
                        'name'     => 'FileContents',
                        'contents' => file_get_contents($path .'/'. $name),
                        'filename' => $name
                    ],
                    [
                        'name'     => 'language',
                        'contents' => 'chs',
                    ],
                    [
                        'name'     => '_token',
                        'contents' => 'Puyj5QjoKq8wzv5FtkhWGTvXWjVoRLgaZeSt0PJi',
                    ]
                ],
            ]);
        } catch (\Exception $e){
            dd($e);
        }
//        dd(json_decode($res->getBody()->getContents(), true));
        $messegeRes = json_decode($res->getBody()->getContents(), true);
        $text = $messegeRes['ParsedResults'][0]['ParsedText'];
        return $text;
    }

    public function show(Request $request){
        info($request->all());
        return $request->all();
    }

}
