<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                {{--<img src="/img/test.png" alt="">--}}
                <div class="title m-b-md">
                </div>
                <div style="font-size:2rem">
                    {{--<b>from OCR: </b>{{$ocr->run()}}--}}
                </div>
                <div style="font-size:2rem">
                    {{--<b>from OCR API: </b>{{$ocrApi}}--}}
                </div>
                <img src="{{$image_src}}" style="width: 400px" alt="">
                <div class="title content" style="font-size: 50px">
                    ocr api : {{$ocr}}
                </div>
                <div class="links">
                    <form action="{{action('OCRController@selectFile')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="image">
                        <button type="submit">ส่ง</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
